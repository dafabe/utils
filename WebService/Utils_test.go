package WebService_test

import (

	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/dafabe/utils/WebService"
	"net/http"
)

func Example_DecodeJson() {

	type Person struct {
		Name string `json:"name"`
		Age  string    `json:"age"`
	}

	var data []Person

	data = append(data, Person{
		Name: "Bob",
		Age:  "35",
	})

	data = append(data, Person{
		Name: "TIM",
		Age:  "2",
	})

	data = append(data, Person{
		Name: "JAKE",
		Age:  "25",
	})

	jsonBody, err := json.Marshal(data)

	if err != nil {
		panic("")
	}

	var result interface{}

	WebService.DecodeJson(jsonBody, &result)

	fmt.Println(result)

	// Output:
	// [map[age:35 name:Bob] map[age:2 name:TIM] map[age:25 name:JAKE]]
}





func Example_GetParams() {

	b := bytes.Buffer{}

	r, err := http.NewRequest("POST", "http://myurl56?stafDept=1&other=hello", &b)

	if err != nil {

	}

	params := WebService.Params(r)

	fmt.Print(params)
	// Output: [1 hello]
}
func Example_GetParams_Empty() {

	b := bytes.Buffer{}

	r, err := http.NewRequest("POST", "http://myurl56", &b)

	if err != nil {

	}

	params := WebService.Params(r)

	fmt.Print(params)
	// Output: []
}
