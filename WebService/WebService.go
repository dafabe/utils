package WebService

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

type WebServicer interface {
	Start()
}

type Scanner interface {
	Scan(rows *sql.Rows)
}

type WebService struct {
	url      string
	database Databaser
	scanner  Scanner
}

func NewWebService(url string, database Databaser, scanner Scanner) *WebService {
	return &WebService{url: url, database: database, scanner: scanner}
}
func (ws *WebService) Start() {

	go http.HandleFunc(ws.url, ws.action())

	// go Listen(port)

}

func Listen(port int) {
	portStr := ":" + strconv.Itoa(port)
	if err := http.ListenAndServe(portStr, nil); err != nil {
		log.Fatal(err)
	}
}
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func (ws *WebService) action() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		rows, err := ws.rows(w, r)
		defer rows.Close()
		ws.scanner.Scan(rows)

		err = json.NewEncoder(w).Encode(ws.scanner)

		CheckErr(err)
	}
}

func iparams(r *http.Request) []interface{} {
	var iparams []interface{}
	params := Params(r)
	for _, param := range params {
		iparams = append(iparams, param)
	}
	return iparams
}

type PostWebService struct {
	WebService
}

func NewPostWebService(url string, database Databaser, scanner Scanner) *PostWebService {
	return &PostWebService{WebService{
		url:      url,
		database: database,
		scanner:  scanner,
	}}
}

type Response struct {
	Success bool `json:"success"`
}

func NewResponse(success bool) *Response {
	return &Response{Success: success}
}
func (ws *PostWebService) Start() {

	go http.HandleFunc(ws.url, ws.action())


}
func (ws *PostWebService) action() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		rows, err := ws.rows(w, r)
		defer rows.Close()
		CheckErr(err)

		ws.writeResponse(w, rows.Next())

	}
}

func (ws *PostWebService) writeResponse(w http.ResponseWriter, success bool) {

	v := NewResponse(success)
	err := json.NewEncoder(w).Encode(v)
	CheckErr(err)
}

func (ws *WebService) rows(w http.ResponseWriter, r *http.Request) (*sql.Rows, error) {
	enableCors(&w)
	dbInfo := ws.database

	db := dbInfo.GetDB()
	defer db.Close()

	query := dbInfo.Query()

	//------------------------------------------------------------------------------
	iparams := iparams(r)
	//------------------------------------------------------------------------------

	rows, err := db.Query(query, iparams...)

	if err != nil {
		fmt.Println(err)
	}
	return rows, err
}
