package WebService

import (
	"context"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Webserverer interface {
	AddWebService()
	Listen()
}

type WebServer struct {
	port        int
	webServices []WebServicer
	svr         http.Server
}

func NewWebServer(port int) *WebServer {
	return &WebServer{port: port}
}

func (w *WebServer) AddWebService(webservicer WebServicer) {
	w.webServices = append(w.webServices, webservicer)
}

func (w *WebServer) start() {
	for _, service := range w.webServices {
		go service.Start()
	}
}

func (w *WebServer) StartAndListen() {

	w.start()
	go w.listen()
}

func (w *WebServer) listen() {
	portStr := ":" + strconv.Itoa(w.port)

	//w.svr = http.Server{
	//	Addr:    portStr,
	//	Handler: nil,
	//}
	//
	//if err := w.svr.ListenAndServe(); err != nil {
	//	log.Fatal(err)
	//}

	if err := http.ListenAndServe(portStr, nil); err != nil {
		log.Fatal(err)
	}
}

func (w *WebServer) Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), 1 * time.Millisecond)
	defer cancel()
	w.svr.Shutdown(ctx)
}
