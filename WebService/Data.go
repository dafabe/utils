package WebService

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

type Databaser interface {
	Query() string
	Args() []interface{}
	GetDB() *sql.DB
}

// Db Info -------------------------------------------------------------------------------------------------------------
type DbInfo struct {
	driver           string
	connectionString string
	query            string
	args             []interface{}
}

func NewDbInfo(driver string, connectionString string, query string, args []interface{}) *DbInfo {
	return &DbInfo{driver: driver, connectionString: connectionString, query: query, args: args}
}

//----------------------------------------------------------------------------------------------------------------------
func (d *DbInfo) Query() string {
	return d.query
}

func (d *DbInfo) Args() []interface{} {
	return d.args
}

func (d *DbInfo) GetDB() *sql.DB {
	// Create connection pool
	db, err := sql.Open(d.driver, d.connectionString)
	if err != nil {
		log.Fatal("Error creating connection pool: " + err.Error())
	}
	return db
}

