package WebService

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

func DecodeJson(b []byte, data interface{}) {

	r := bytes.NewBuffer(b)

	decoder := json.NewDecoder(r)

	decoder.Decode(&data)
}

func Get(url string) string {

	resp, err := http.Get(url)

	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {

		log.Fatal(err)
	}

	return strings.TrimSuffix(string(body), "\n")
}

func CheckErr(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func Params(r *http.Request) []string {
	splitSlice := strings.Split(r.URL.String(), "?")

	var split string
	var output []string
	if len(splitSlice) > 1 {
		split = splitSlice[1]

		next := strings.Split(split, "&")
		var paramHeadders []string
		for _, s := range next {
			paramHeadders = append(paramHeadders, strings.Split(s, "=")[0])
		}

		q := r.URL.Query()


		for _, headder := range paramHeadders {
			output = append(output, q.Get(headder))
		}
		return output
	}

	return nil
}
