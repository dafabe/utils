package WebService

import (
	"database/sql"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"regexp"
)

// This is a sample of what must be created to generate a new web service
//-----------------------------------------------------------------------------------------------------------------
type Person struct {
	Name string `json:"name"`
	Age  string `json:"age"`
}

type Dog struct {
	Name  string `json:"name"`
	Age   string `json:"age"`
	Owner string `json:"owner"`
}

type Dogs []Dog

func (d *Dogs) Scan(rows *sql.Rows) {

	for rows.Next() {
		dog := Dog{}
		err := rows.Scan(
			&dog.Name,
			&dog.Age,
			&dog.Owner,
		)
		*d = append(*d, dog)
		CheckErr(err)
	}

	CheckErr(rows.Err())

}

//-----------------------------------------------------------------------------------------------------
type People []Person

func (p *People) Scan(rows *sql.Rows) {

	for rows.Next() {
		person := Person{}
		err := rows.Scan(
			&person.Name,
			&person.Age,
		)
		*p = append(*p, person)
		CheckErr(err)
	}

	CheckErr(rows.Err())

}

//---------------------------------------------------------------------------------------------------------------
type StubDbInfo struct {
}

func (s *StubDbInfo) Query() string {
	return "select name, age, owner from dogs where owner = ?"
}

func (s *StubDbInfo) Args() []interface{} {
	panic("implement me")
}

func (s *StubDbInfo) GetDB() *sql.DB {
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}

	rs1 := sqlmock.NewRows([]string{"name", "age"})
	rs1.AddRow("Bob", "35")
	rs1.AddRow("TIM", "2")
	rs1.AddRow("JAKE", "25")

	query := s.Query()
	meta := regexp.QuoteMeta(query)

	mock.ExpectQuery(meta).
		WillReturnRows(rs1)

	return db
}

//---------------------------------------------------------------------------------------------------------------
type DogDbInfo struct {
}

func (s *DogDbInfo) Query() string {
	return "select name, age, owner from dogs where owner = ?"
}

func (s *DogDbInfo) Args() []interface{} {
	panic("implement me")
}

func (s *DogDbInfo) GetDB() *sql.DB {
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}

	rs1 := sqlmock.NewRows([]string{"name", "age", "owner"})
	rs1.AddRow("rex", "1", "ted")
	rs1.AddRow("jet", "2", "ted")
	rs1.AddRow("max", "3", "ted")

	query := s.Query()
	meta := regexp.QuoteMeta(query)

	mock.ExpectQuery(meta).
		WithArgs("ted").
		WillReturnRows(rs1)

	return db
}

// ---------------------------------------------------------------------------------------------

type DogDbInfo2 struct {
	DogDbInfo
}

func (s *DogDbInfo2) Query() string {
	return "select name, age, owner from dogs where owner = ? and name = ?"
}

func (s *DogDbInfo2) GetDB() *sql.DB {
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}

	rs1 := sqlmock.NewRows([]string{"name", "age", "owner"})
	rs1.AddRow("rex", "1", "ted")

	query := s.Query()
	meta := regexp.QuoteMeta(query)

	mock.ExpectQuery(meta).
		WithArgs("ted", "rex").
		WillReturnRows(rs1)

	return db
}

//--------------------------------------------------------------------------------------------------------------

type DogDbInfo0 struct {
	DogDbInfo
}

func (s *DogDbInfo0) Query() string {
	return "select name, age, owner from dogs"
}

func (s *DogDbInfo0) GetDB() *sql.DB {
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}

	rs1 := sqlmock.NewRows([]string{"name", "age", "owner"})
	rs1.AddRow("rex", "1", "ted")
	rs1.AddRow("jet", "2", "ted")
	rs1.AddRow("max", "3", "ted")

	query := s.Query()
	meta := regexp.QuoteMeta(query)

	mock.ExpectQuery(meta).
		WillReturnRows(rs1)

	return db
}

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}
type Users []User

func (u *Users) Scan(rows *sql.Rows) {

	for rows.Next() {
		user := User{}
		err := rows.Scan(
			&user.Username,
			&user.Password,
		)
		*u = append(*u, user)
		CheckErr(err)
	}

	CheckErr(rows.Err())

}

type UsernamePassword struct {
	DogDbInfo
}

func (s *UsernamePassword) Query() string {
	return "select * from users where username = ? and password = ?"
}

func (s *UsernamePassword) GetDB() *sql.DB {
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}

	rs1 := sqlmock.NewRows([]string{"username", "password"})
	rs1.AddRow("admin", "admin")

	query := s.Query()
	meta := regexp.QuoteMeta(query)

	mock.ExpectQuery(meta).
		WillReturnRows(rs1)

	return db
}

type UsernamePasswordNill struct {
	UsernamePassword
}

func (s *UsernamePasswordNill) GetDB() *sql.DB {
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}

	rs1 := sqlmock.NewRows([]string{"username", "password"})

	query := s.Query()
	meta := regexp.QuoteMeta(query)

	mock.ExpectQuery(meta).
		WillReturnRows(rs1)

	return db
}

//------------------------------------------------------------------------------------------
type UserRecordDbInfo struct {
	DogDbInfo
}

func (s *UserRecordDbInfo) Query() string {
	return "INSERT INTO `user` (role_id, user_name, description,`password`) VALUES (5, 'boba', 'drovers', 'pass'); SELECT id, role_id, user_name, description FROM `user` WHERE Id=(SELECT LAST_INSERT_ID());"
}

func (s *UserRecordDbInfo) GetDB() *sql.DB {
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}

	rs1 := sqlmock.NewRows([]string{"id", "user_name", "description", "password"})
	rs1.AddRow("15", "4", "ted desc", "password")

	query := s.Query()
	meta := regexp.QuoteMeta(query)

	mock.ExpectQuery(meta).
		WillReturnRows(rs1)

	return db
}

type UserRecordDbInfoFail struct {
	UserRecordDbInfo
}

func (s *UserRecordDbInfoFail) GetDB() *sql.DB {
	db, mock, err := sqlmock.New()
	if err != nil {
		fmt.Printf("an error '%s' was not expected when opening a stub database connection", err)
	}

	rs1 := sqlmock.NewRows([]string{"id", "user_name", "description", "password"})
	//rs1.AddRow("15", "4", "ted desc", "password")

	query := s.Query()
	meta := regexp.QuoteMeta(query)

	mock.ExpectQuery(meta).
		WillReturnRows(rs1)

	return db
}

type UserRecord struct {
	Id          string `json:"id"`
	RoleId      string `json:"role_id"`
	Description string `json:"description"`
	Username    string `json:"username"`
}
type UserRecords []UserRecord

func (u *UserRecords) Scan(rows *sql.Rows) {

	for rows.Next() {
		user := UserRecord{}
		err := rows.Scan(
			&user.Id,
			&user.RoleId,
			&user.Description,
			&user.Username,
		)
		*u = append(*u, user)
		CheckErr(err)
	}

	CheckErr(rows.Err())

}

//-------------------------------------------------------------------------------------------
