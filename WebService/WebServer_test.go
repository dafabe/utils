package WebService_test

import (

	"fmt"
	"gitlab.com/dafabe/utils/WebService"
)

//



func Example_WebServer() {

	ws := WebService.NewWebService("/myurl9", &WebService.StubDbInfo{}, &WebService.People{})
	ws2 := WebService.NewWebService("/myurl4", &WebService.StubDbInfo{}, &WebService.People{})



	wsvr := WebService.NewWebServer(3052)
	wsvr.AddWebService(ws)
	wsvr.AddWebService(ws2)
	wsvr.StartAndListen()

	resp := WebService.Get("http://localhost:3052/myurl9")

	fmt.Print(resp)

	resp2 := WebService.Get("http://localhost:3052/myurl4")
fmt.Println()
	fmt.Print(resp2)


	// Output:
	// [{"name":"Bob","age":"35"},{"name":"TIM","age":"2"},{"name":"JAKE","age":"25"}]
	// [{"name":"Bob","age":"35"},{"name":"TIM","age":"2"},{"name":"JAKE","age":"25"}]
}


//
//func Example_WebServer3() {
//
//	ws := WebService.NewWebService("/myurl22", &WebService.StubDbInfo{}, &WebService.People{})
//	ws2 := WebService.NewWebService("/myurl35", &WebService.StubDbInfo{}, &WebService.People{})
//
//
//
//	wsvr := WebService.NewWebServer(3050)
//	wsvr.AddWebService(ws)
//	wsvr.StartAndListen()
//
//	wsvr2 := WebService.NewWebServer(3050)
//	wsvr2.AddWebService(ws2)
//	wsvr.StartAndListen()
//
//	resp := WebService.Get("http://localhost:3050/myurl22")
//
//	fmt.Print(resp)
//
//	resp2 := WebService.Get("http://localhost:3050/myurl35")
//
//	fmt.Print(resp2)
//
//
//	// Output:
//	// [{"name":"Bob","age":"35"},{"name":"TIM","age":"2"},{"name":"JAKE","age":"25"}]
//	// [{"name":"Bob","age":"35"},{"name":"TIM","age":"2"},{"name":"JAKE","age":"25"}]
//}